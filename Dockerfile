ARG ALPINE_VERSION=3.16

FROM alpine:${ALPINE_VERSION} as builder

ARG TIG_VERSION=2.5.7

RUN apk add --no-cache \
        asciidoc \
        build-base \
        curl \
        ncurses-dev \
        tar \
        xmlto && \
    curl -fLo tig-${TIG_VERSION}.tar.gz \
        https://github.com/jonas/tig/releases/download/tig-${TIG_VERSION}/tig-${TIG_VERSION}.tar.gz && \
    tar xzf tig-${TIG_VERSION}.tar.gz && \
    cd tig-${TIG_VERSION} && \
    ./configure --prefix=/usr/local && \
    make all && make install


FROM alpine:${ALPINE_VERSION}

COPY --from=builder /usr/local/bin/tig /usr/local/bin/
COPY --from=builder /usr/local/etc/tigrc /etc/
RUN apk add --no-cache \
        git \
        git-lfs \
        ncurses-libs \
        ncurses-terminfo \
        netcat-openbsd \
        openssh-client-default

CMD ["tig"]
